import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AuctionEntity } from './auction.entity';
import { CreateAuctionDto } from './dto/create-auction.dto';
import { AuctionRO, AuctionData } from './auction.interface';
import { validate } from 'class-validator';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { HttpStatus } from '@nestjs/common';

@Injectable()
export class AuctionService {
  constructor(
    @InjectRepository(AuctionEntity)
    private readonly auctionRepository: Repository<AuctionEntity>,
  ) {}

  async findAll(): Promise<AuctionEntity[]> {
    return await this.auctionRepository.find();
  }

  async create(dto: CreateAuctionDto): Promise<AuctionData> {
    const { auctionDate, auctionURL, isActiveStatistic } = dto;

    const newAuction = new AuctionEntity();
    newAuction.auctionDate = auctionDate;
    newAuction.auctionURL = auctionURL;
    newAuction.isActiveStatistic = isActiveStatistic;
    const errors = await validate(newAuction);
    if (errors.length > 0) {
      throw new HttpException(
        { message: 'Input data validation failed' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const savedAuction = await this.auctionRepository.save(newAuction);
      return savedAuction;
    }
  }
}
