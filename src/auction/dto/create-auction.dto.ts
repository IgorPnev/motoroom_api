export class CreateAuctionDto {
  readonly auctionDate: Date;
  readonly auctionURL: string;
  readonly isActiveStatistic: boolean;
}
