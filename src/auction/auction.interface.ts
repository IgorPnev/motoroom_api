export interface AuctionData {
  auctionDate: Date;
  auctionURL: string;
  isActiveStatistic: boolean;
}

export interface AuctionRO {
  auction: AuctionData;
}
