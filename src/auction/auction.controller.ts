import { Controller, Get, Post, Body } from '@nestjs/common';
import { AuctionService } from './auction.service';
import { AuctionEntity } from './auction.entity';
import { CreateAuctionDto } from './dto/create-auction.dto';

@Controller('auction')
export class AuctionController {
  constructor(private readonly auctionService: AuctionService) {}
  @Get('auctions')
  getAllUsers(): Promise<AuctionEntity[]> {
    return this.auctionService.findAll();
  }

  @Post('auctions')
  async create(@Body() auctionData: CreateAuctionDto) {
    return this.auctionService.create(auctionData);
  }
}
