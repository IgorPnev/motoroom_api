import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  UpdateDateColumn,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';
import { BikeEntity } from 'src/bike/entity/bike.entity';

@Entity('auction')
export class AuctionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ name: 'create_ts' })
  create: Date;

  @UpdateDateColumn({ name: 'update_ts', nullable: true })
  update: Date = undefined;

  @Column()
  auctionDate: Date;

  @Column()
  auctionURL: string = undefined;

  @Column({ name: 'is_active_statistic', default: true })
  isActiveStatistic: boolean = undefined;

  // @OneToMany(type => BikeEntity, bikes => bikes.auction, { eager: true })
  // bikes: BikeEntity[];
}
