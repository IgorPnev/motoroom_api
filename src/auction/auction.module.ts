import { Module } from '@nestjs/common';
import { AuctionService } from './auction.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuctionEntity } from './auction.entity';
import { AuctionController } from './auction.controller';

@Module({
  imports: [TypeOrmModule.forFeature([AuctionEntity])],
  providers: [AuctionService],
  controllers: [AuctionController],
})
export class AuctionModule {}
