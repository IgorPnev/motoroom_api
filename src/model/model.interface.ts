import { ModelEntity } from './model.entity';
import { ModelAliasEntity } from './alias.entity';
import { BrandAliasEntity } from 'src/brand/alias.entity';

export interface ModelsRO {
  model: ModelEntity[];
}

export interface AliasRO {
  alias: ModelAliasEntity;
  brand: BrandAliasEntity;
}
