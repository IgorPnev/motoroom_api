import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { ModelEntity } from './model.entity';
import { BrandAliasEntity } from '../brand/alias.entity';

@Entity('model_alias')
export class ModelAliasEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(type => ModelEntity, models => models.alias, { eager: true })
  models: ModelEntity[];
}
