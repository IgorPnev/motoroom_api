import { Controller, Get, Post, Body } from '@nestjs/common';
import { ModelService } from './model.service';
import { ModelEntity } from './model.entity';
import { ModelAliasEntity } from './alias.entity';
import { CreateAliasDto } from './dto/create-alias.dto';
import { CreateModelDto } from './dto/create-model.dto';

@Controller('model')
export class ModelController {
  constructor(private readonly modelService: ModelService) {}

  @Get('models')
  getAllModels(): Promise<ModelEntity[]> {
    return this.modelService.findModelAll();
  }

  @Post('models')
  async createModel(@Body() modelData: CreateModelDto) {
    return this.modelService.createModel(modelData);
  }

  @Get('models_alias')
  getAllAlias(): Promise<ModelAliasEntity[]> {
    return this.modelService.findAliasModelAll();
  }

  @Post('models_alias')
  async createModelAlias(@Body() aliasData: CreateAliasDto) {
    return this.modelService.createModelAlias(aliasData);
  }
}
