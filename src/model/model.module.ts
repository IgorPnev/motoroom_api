import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ModelEntity } from './model.entity';
import { ModelAliasEntity } from './alias.entity';
import { ModelService } from './model.service';
import { BrandEntity } from 'src/brand/brand.entity';
import { ModelController } from './model.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([ModelEntity, ModelAliasEntity, BrandEntity]),
  ],
  providers: [ModelService],
  controllers: [ModelController],
})
export class ModelModule {}
