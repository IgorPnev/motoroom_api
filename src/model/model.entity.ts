import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

import { ModelAliasEntity } from './alias.entity';
import { BrandEntity } from 'src/brand/brand.entity';

@Entity('model')
export class ModelEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(type => ModelAliasEntity, alias => alias.models)
  @JoinColumn({ name: 'model_alias_id' })
  alias: ModelAliasEntity;

  @ManyToOne(type => BrandEntity, brand => brand.models)
  @JoinColumn({ name: 'brand_id' })
  brand: BrandEntity;
}
