import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ModelEntity } from './model.entity';
import { ModelAliasEntity } from './alias.entity';

import { validate } from 'class-validator';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { HttpStatus } from '@nestjs/common';
import { CreateAliasDto } from './dto/create-alias.dto';
import { CreateModelDto } from './dto/create-model.dto';
import { BrandEntity } from 'src/brand/brand.entity';

@Injectable()
export class ModelService {
  constructor(
    @InjectRepository(ModelEntity)
    private readonly modelRepository: Repository<ModelEntity>,
    @InjectRepository(ModelAliasEntity)
    private readonly modelAliasRepository: Repository<ModelAliasEntity>,
    @InjectRepository(BrandEntity)
    private readonly brandRepository: Repository<BrandEntity>,
  ) {}

  async findModelAll(): Promise<ModelEntity[]> {
    return await this.modelRepository.find();
  }

  async findAliasModelAll(): Promise<ModelAliasEntity[]> {
    return await this.modelAliasRepository.find();
  }

  async createModelAlias(dto: CreateAliasDto): Promise<ModelAliasEntity> {
    const { name } = dto;

    const newModelAlias = new ModelAliasEntity();
    newModelAlias.name = name;

    const errors = await validate(newModelAlias);
    if (errors.length > 0) {
      throw new HttpException(
        { message: 'Input data validation failed' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const savedModelAlias = await this.modelAliasRepository.save(
        newModelAlias,
      );
      return savedModelAlias;
    }
  }

  async createModel(dto: CreateModelDto): Promise<ModelEntity> {
    const { name, alias, brand } = dto;

    const aliasEntry = await this.modelAliasRepository.findOne(alias);
    const brandRecord = await this.brandRepository.findOne(brand);
    const newModel = new ModelEntity();
    newModel.name = name;
    newModel.alias = aliasEntry;
    newModel.brand = brandRecord;

    const errors = await validate(newModel);
    if (errors.length > 0) {
      throw new HttpException(
        { message: 'Input data validation failed' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const savedModel = await this.modelRepository.save(newModel);
      return savedModel;
    }
  }
}
