export class CreateModelDto {
  readonly name: string;
  readonly alias: string;
  readonly brand: string;
}
