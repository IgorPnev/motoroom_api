export class CreateAliasDto {
  readonly name: string;
  readonly logo: string;
}
