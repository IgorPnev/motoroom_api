export class CreateBrandDto {
  readonly name: string;
  readonly alias: string;
}
