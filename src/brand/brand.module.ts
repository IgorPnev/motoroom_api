import { Module } from '@nestjs/common';
import { BrandService } from './brand.service';
import { BrandEntity } from './brand.entity';
import { BrandAliasEntity } from './alias.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BrandController } from './brand.controller';

@Module({
  imports: [TypeOrmModule.forFeature([BrandEntity, BrandAliasEntity])],
  providers: [BrandService],
  controllers: [BrandController],
})
export class BrandModule {}
