import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BrandEntity } from './brand.entity';
import { BrandAliasEntity } from './alias.entity';

import { validate } from 'class-validator';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { HttpStatus } from '@nestjs/common';
import { CreateAliasDto } from './dto/create-alias.dto';
import { CreateBrandDto } from './dto/create-brand.dto';

@Injectable()
export class BrandService {
  constructor(
    @InjectRepository(BrandEntity)
    private readonly brandRepository: Repository<BrandEntity>,
    @InjectRepository(BrandAliasEntity)
    private readonly brandAliasRepository: Repository<BrandAliasEntity>,
  ) {}

  async findBrandAll(): Promise<BrandEntity[]> {
    return await this.brandRepository.find();
  }

  async findAliasBrandAll(): Promise<BrandAliasEntity[]> {
    return await this.brandAliasRepository.find();
  }

  async createBrandAlias(dto: CreateAliasDto): Promise<BrandAliasEntity> {
    const { name, logo } = dto;

    const newBrandAlias = new BrandAliasEntity();
    newBrandAlias.name = name;
    newBrandAlias.logo = logo;

    const errors = await validate(newBrandAlias);
    if (errors.length > 0) {
      throw new HttpException(
        { message: 'Input data validation failed' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const savedBrandAlias = await this.brandAliasRepository.save(
        newBrandAlias,
      );
      return savedBrandAlias;
    }
  }

  async createBrand(dto: CreateBrandDto): Promise<BrandEntity> {
    const { name, alias } = dto;

    const aliasEntry = await this.brandAliasRepository.findOne(alias);
    console.log(alias);
    const newBrand = new BrandEntity();

    newBrand.name = name;
    newBrand.alias = aliasEntry;

    const errors = await validate(newBrand);
    if (errors.length > 0) {
      throw new HttpException(
        { message: 'Input data validation failed' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const savedBrand = await this.brandRepository.save(newBrand);
      return savedBrand;
    }
  }
}
