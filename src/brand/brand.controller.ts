import { Controller, Get, Post, Body } from '@nestjs/common';
import { BrandService } from './brand.service';
import { BrandEntity } from './brand.entity';
import { BrandAliasEntity } from './alias.entity';
import { CreateAliasDto } from './dto/create-alias.dto';
import { CreateBrandDto } from './dto/create-brand.dto';

@Controller('brand')
export class BrandController {
  constructor(private readonly brandService: BrandService) {}

  @Get('brands')
  getAllBrands(): Promise<BrandEntity[]> {
    return this.brandService.findBrandAll();
  }

  @Post('brands')
  async createBrand(@Body() brandData: CreateBrandDto) {
    return this.brandService.createBrand(brandData);
  }

  @Get('brands_alias')
  getAllAlias(): Promise<BrandAliasEntity[]> {
    return this.brandService.findAliasBrandAll();
  }

  @Post('brands_alias')
  async createBrandAlias(@Body() aliasData: CreateAliasDto) {
    return this.brandService.createBrandAlias(aliasData);
  }
}
