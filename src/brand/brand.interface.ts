import { BrandEntity } from './brand.entity';
import { BrandAliasEntity } from './alias.entity';

export interface BrandsRO {
  brand: BrandEntity[];
}

export interface AliasRO {
  alias: BrandAliasEntity;
}
