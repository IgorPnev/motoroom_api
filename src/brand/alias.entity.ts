import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  UpdateDateColumn,
  CreateDateColumn,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { BrandEntity } from './brand.entity';
import { ModelAliasEntity } from 'src/model/alias.entity';
@Entity('brand_alias')
export class BrandAliasEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  logo: string = undefined;

  @OneToMany(type => BrandEntity, brands => brands.alias, { eager: true })
  brands: BrandEntity[];
}
