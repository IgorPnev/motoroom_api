import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import { BrandAliasEntity } from './alias.entity';
import { ModelEntity } from 'src/model/model.entity';

@Entity('brand')
export class BrandEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(type => BrandAliasEntity, alias => alias.brands)
  @JoinColumn({ name: 'brand_alias_id' })
  alias: BrandAliasEntity;

  @OneToMany(type => ModelEntity, models => models.brand, { eager: true })
  models: ModelEntity[];
}
