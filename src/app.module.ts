import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BikeModule } from './bike/bike.module';
import { AuctionModule } from './auction/auction.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { BrandModule } from './brand/brand.module';
import { ModelModule } from './model/model.module';

@Module({
  controllers: [AppController],
  providers: [AppService],
  imports: [
    TypeOrmModule.forRoot(),
    AuctionModule,
    BrandModule,
    ModelModule,
    BikeModule,
  ],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
