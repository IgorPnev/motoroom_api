import { Controller, Get, Post, Body, ValidationPipe } from '@nestjs/common';
import { BikeEntity } from './entity/bike.entity';
import { BikeService } from './bike.service';
import { CreateBikeDto } from './dto/create-bike.dto';

@Controller('bike')
export class BikeController {
  constructor(private readonly bikeService: BikeService) {}
  @Get('bikes')
  getBikes(): Promise<BikeEntity[]> {
    return this.bikeService.findAll();
  }

  @Post('bikes')
  async create(@Body() body: CreateBikeDto) {
    return this.bikeService.create(body);
  }
}
