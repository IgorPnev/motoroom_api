import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BikeEntity } from './entity/bike.entity';
import { CityEntity } from './entity/city.entity';
import { DetailEntity } from './entity/detail.entity';
import { ImageEntity } from './entity/image.entity';
import { RankEntity } from './entity/rank.entity';
import { VideoEntity } from './entity/video.entity';
import { AuctionEntity } from 'src/auction/auction.entity';
import { BrandEntity } from 'src/brand/brand.entity';
import { ModelEntity } from 'src/model/model.entity';
import { BikeService } from './bike.service';
import { BikeController } from './bike.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CityEntity,
      DetailEntity,
      ImageEntity,
      RankEntity,
      VideoEntity,
      AuctionEntity,
      BrandEntity,
      ModelEntity,
      BikeEntity,
    ]),
  ],
  providers: [BikeService],
  controllers: [BikeController],
})
export class BikeModule {}
