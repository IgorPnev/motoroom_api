import { Video, Image, Detail } from '../bike.interface';
import { BrandEntity } from 'src/brand/brand.entity';

export class CreateBikeDto {
  readonly brand: string;
  readonly model: string;
  readonly bidId: number;
  readonly url: string;
  readonly lastBid: number;
  readonly startingBid: number;
  readonly auction: string;
  readonly city: string;
  readonly isSold: boolean;
  readonly preview: string;
  readonly volume: number;
  readonly color: string;
  readonly meter: number;
  readonly engineModel: string;
  readonly frameNo: string;
  readonly details: Detail[];
  readonly pics: Image[];
  readonly videos: Video[];
  readonly rank: {
    frame: number;
    es: number;
    exterior: number;
    rear: number;
    front: number;
    engine: number;
    average: number;
  };
}
