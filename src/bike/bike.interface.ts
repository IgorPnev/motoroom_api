import { ImageType } from './entity/video.entity';
import { BikeEntity } from './entity/bike.entity';
import { BrandEntity } from 'src/brand/brand.entity';
import { ModelEntity } from 'src/model/model.entity';
import { AuctionEntity } from 'src/auction/auction.entity';
import { CityEntity } from './entity/city.entity';
import { DetailEntity } from './entity/detail.entity';

export interface Video {
  poster: string;
  mp4: string;
  ogv: string;
  webm: string;
}

export interface Image {
  url: string;
  type: ImageType;
}

export interface Detail {
  url: string;
  description: string;
}

export interface BikeRO {
  brand: BrandEntity;
  model: ModelEntity;
  bidId: number;
  url: string;
  lastBid: number;
  startingBid: number;
  auction: AuctionEntity;
  city: CityEntity;
  isSold: boolean;
  preview: string;
  volume: number;
  color: string;
  meter: number;
  engineModel: string;
  frameNo: string;
  details: DetailEntity;
  pics: Image[];
  videos: Video[];
  rank: {
    frame: number;
    es: number;
    exterior: number;
    rear: number;
    front: number;
    engine: number;
    average: number;
  };
}
