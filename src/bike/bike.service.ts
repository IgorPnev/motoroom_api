import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { validate } from 'class-validator';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { HttpStatus } from '@nestjs/common';
import { BikeEntity } from './entity/bike.entity';
import { CreateBikeDto } from './dto/create-bike.dto';
import { BikeRO } from './bike.interface';
import { BrandEntity } from 'src/brand/brand.entity';

@Injectable()
export class BikeService {
  constructor(
    @InjectRepository(BikeEntity)
    private readonly bikeRepository: Repository<BikeEntity>,
    @InjectRepository(BrandEntity)
    private readonly brandRepository: Repository<BrandEntity>,
  ) {}

  async findAll(): Promise<BikeEntity[]> {
    return await this.bikeRepository.find();
  }

  async create(dto: CreateBikeDto): Promise<BikeRO> {
    const { brand, color, bidId, url, lastBid } = dto;
    const brandEntry = await this.brandRepository.findOne(brand);
    const newBike = new BikeEntity();
    newBike.color = color;
    newBike.bidId = bidId;
    newBike.url = url;
    newBike.lastBid = lastBid;

    const errors = await validate(newBike);
    if (errors.length > 0) {
      throw new HttpException(
        { message: 'Input data validation failed' },
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const savedBike = await this.bikeRepository.save(newBike);
      return savedBike;
    }
  }
}
