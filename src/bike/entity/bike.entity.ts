import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
} from 'typeorm';

import { AuctionEntity } from 'src/auction/auction.entity';
import { DetailEntity } from './detail.entity';
import { ImageEntity } from './image.entity';
import { RankEntity } from './rank.entity';
import { VideoEntity } from './video.entity';
import { BrandEntity } from 'src/brand/brand.entity';
import { ModelEntity } from 'src/model/model.entity';
import { CityEntity } from './city.entity';

@Entity('bike')
export class BikeEntity {
  @PrimaryGeneratedColumn({ name: 'bike_id' })
  id: number;

  @CreateDateColumn({ name: 'create_ts' })
  create: Date;

  @UpdateDateColumn({ name: 'update_ts', nullable: true })
  update: Date = undefined;

  @OneToOne(type => BrandEntity)
  @JoinColumn({ name: 'brand_id' })
  brand: BrandEntity;

  @OneToOne(type => ModelEntity)
  @JoinColumn({ name: 'model_id' })
  model: ModelEntity;

  @Column({ name: 'bid_id' })
  bidId: number;

  @Column()
  url: string;

  @Column({ name: 'last_bid' })
  lastBid: number;

  @Column({ name: 'starting_bid' })
  startingBid: number;

  // @OneToMany(type => AuctionEntity, auction => auction.bikes)
  @OneToOne(type => AuctionEntity)
  @JoinColumn({ name: 'auction_id' })
  auction: AuctionEntity;

  @OneToOne(type => CityEntity)
  @JoinColumn({ name: 'city_id' })
  city: CityEntity;

  @Column({ name: 'is_sold', default: false })
  isSold: boolean;

  @Column()
  preview: string;

  @Column()
  volume: number;

  @Column()
  color: string;

  @Column()
  meter: number;

  @Column({ name: 'engine_model' })
  engineModel: string;

  @Column({ name: 'frame_no' })
  frameNo: string;

  @OneToOne(type => DetailEntity)
  @JoinColumn({ name: 'details_id' })
  details: DetailEntity;

  @OneToMany(type => ImageEntity, pics => pics.bike, { eager: true })
  pics: ImageEntity[];

  @OneToMany(type => VideoEntity, videos => videos.bike, { eager: true })
  videos: VideoEntity[];

  @OneToOne(type => RankEntity)
  @JoinColumn({ name: 'rank_id' })
  rank: RankEntity;
}
