import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { BikeEntity } from './bike.entity';

export enum ImageType {
  FRAME = 'frame',
  ES = 'es',
  REAR = 'rear',
  EXTERIOR = 'exterior',
  FRONT = 'front',
  ENGINE = 'engine',
  GENERAL = 'general',
}

@Entity('image')
export class ImageEntity {
  @PrimaryGeneratedColumn({ name: 'image_id' })
  id: number;

  @Column()
  url: string;

  @Column({
    type: 'enum',
    enum: ImageType,
    default: ImageType.GENERAL,
  })
  type: ImageType;

  @ManyToOne(type => BikeEntity, bike => bike.pics)
  @JoinColumn({ name: 'bike_id' })
  bike: BikeEntity;
}
