import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('city')
export class CityEntity {
  @PrimaryGeneratedColumn({ name: 'city_id' })
  id: number;

  @Column()
  city: string;
}
