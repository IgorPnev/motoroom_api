import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('rank')
export class RankEntity {
  @PrimaryGeneratedColumn({ name: 'detail_id' })
  id: number;
  @Column()
  frame: number;
  @Column()
  es: number;
  @Column()
  rear: number;
  @Column()
  exterior: number;
  @Column()
  front: number;
  @Column()
  engine: number;
  @Column()
  average: number;
}
