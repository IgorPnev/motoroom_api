import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { BikeEntity } from './bike.entity';

export enum ImageType {
  FRAME = 'frame',
  ES = 'es',
  REAR = 'rear',
  EXTERIOR = 'exterior',
  FRONT = 'front',
  ENGINE = 'engine',
  GENERAL = 'general',
}

@Entity('video')
export class VideoEntity {
  @PrimaryGeneratedColumn({ name: 'video_id' })
  id: number;

  @Column()
  poster: string;

  @Column()
  mp4: string;

  @Column()
  ogv: string;

  @Column()
  webm: string;

  @ManyToOne(type => BikeEntity, bike => bike.pics)
  @JoinColumn({ name: 'bike_id' })
  bike: BikeEntity;
}
