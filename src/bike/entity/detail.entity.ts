import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { BikeEntity } from './bike.entity';

@Entity('details')
export class DetailEntity {
  @PrimaryGeneratedColumn({ name: 'detail_id' })
  id: number;

  @Column()
  type: string;

  @Column({ type: 'json' })
  description: JSON;
}
